package opensavvy.spine.typed

import kotlin.jvm.JvmInline
import kotlin.reflect.KProperty

typealias ParameterStorage = MutableMap<String, String>
typealias ParameterConstructor<P> = (ParameterStorage) -> P

/**
 * Additional parameters for [endpoints][Endpoint].
 *
 * Endpoints declare mandatory parameters, such as the identifier of the resource.
 * This abstract class allows to declare additional parameters in a type-safe manner.
 *
 * ### Usage
 *
 * In common code, declare a class that inherits from [Parameters], and use it to declare the name and type of the available parameters:
 * ```kotlin
 * class MyRequestParameters : Parameters() {
 *     var param1: String? by parameter()
 *     var isSubscribed: Boolean by parameter("is_subscribed", default = false)
 * }
 * ```
 *
 * To create a new parameter bundle, use the standard library [apply] function:
 * ```kotlin
 * val example = MyRequestParameters().apply {
 *     param1 = "value"
 *     isSubscribed = true
 * }
 * ```
 *
 * The values can be accessed in a type-safe manner:
 * ```kotlin
 * println(example.param1)
 * ```
 * The values can also be accessed via their string representation:
 * ```kotlin
 * println(example.data["param1"])
 * ```
 */
abstract class Parameters(
	/**
	 * Internal string representation of the parameters.
	 */
	val data: ParameterStorage = HashMap(),
) {

	/**
	 * Declares an optional parameter of type [T].
	 *
	 * If the parameter is missing, reading it will return [default] instead.
	 */
	fun <T : Any> parameter(default: T) = Parameter(default)

	/**
	 * Declares an optional parameter [name] of type [T].
	 *
	 * If the parameter is missing, reading it will return [default] instead.
	 */
	fun <T : Any> parameter(name: String, default: T) = NamedParameter(name, default).also { validateName(name) }

	/**
	 * Declares a parameter of type [T].
	 *
	 * If [T] is nullable, this function declares an optional parameter with a default value of `null`.
	 * If [T] is non-nullable, this function declares a mandatory parameter which will throw a [NoSuchElementException] if no value is provided.
	 */
	fun <T : Any?> parameter() = Parameter<T>(null)

	/**
	 * Declares a parameter [name] of type [T].
	 *
	 * If [T] is nullable, this function declares an optional parameter with a default value of `null`.
	 * If [T] is non-nullable, this function declares a mandatory parameter which will throw a [NoSuchElementException] if no value is provided.
	 */
	fun <T : Any?> parameter(name: String) = NamedParameter<T>(name, null).also { validateName(name) }

	// Internal types used to store the name of a parameter
	// End users should not need to use them directly
	@JvmInline
	value class Parameter<T>(val defaultValue: T?)
	class NamedParameter<T>(val name: String, val defaultValue: T?)

	/**
	 * The default parameter instance.
	 *
	 * Use this instance for operations that take no parameters.
	 */
	object Empty : Parameters()
}

inline operator fun <T> Parameters.Parameter<T>.provideDelegate(thisRef: Parameters, property: KProperty<*>): Parameters.Parameter<T> {
	validateName(property.name)
	return this
}

// Accesses the value and converts it from a String
// This is what the 'by' keyword calls when reading from the value
// Everything it does is explained in the 'parameter' documentation
inline operator fun <reified T> Parameters.Parameter<T>.getValue(thisRef: Parameters, property: KProperty<*>): T {
	val value = thisRef.data[property.name] ?: return run {
		if (defaultValue is T)
			defaultValue
		else
			throw NoSuchElementException("The parameter '${property.name}' is mandatory, but no value was provided.")
	}

	return convertValue(value)
}

inline operator fun <reified T> Parameters.NamedParameter<T>.getValue(thisRef: Parameters, property: KProperty<*>): T {
	val value = thisRef.data[name] ?: return run {
		if (defaultValue is T)
			defaultValue
		else
			throw NoSuchElementException("The parameter '${name}' is mandatory, but no value was provided.")
	}

	return convertValue(value)
}

// Writes the value and converts it to a String
// This is what the 'by' keyword calls when writing to the value
// Everything it does is explained in the 'parameter' documentation
inline operator fun <T> Parameters.Parameter<T>.setValue(thisRef: Parameters, property: KProperty<*>, value: T) {
	thisRef.data[property.name] = stringifyValue(value)
}

inline operator fun <T> Parameters.NamedParameter<T>.setValue(thisRef: Parameters, property: KProperty<*>, value: T) {
	thisRef.data[name] = stringifyValue(value)
}

fun <P : Parameters> buildParameters(construct: ParameterConstructor<P>, block: P.() -> Unit): P {
	return construct(HashMap()).apply(block)
}

@PublishedApi
internal inline fun <reified T> convertValue(value: String): T = when (T::class) {
	String::class -> value as T
	Boolean::class -> value.toBooleanStrict() as T
	Byte::class -> value.toByte() as T
	Short::class -> value.toShort() as T
	Int::class -> value.toInt() as T
	Long::class -> value.toLong() as T
	UByte::class -> value.toUByte() as T
	UShort::class -> value.toUShort() as T
	UInt::class -> value.toUInt() as T
	ULong::class -> value.toULong() as T
	Float::class -> value.toFloat() as T
	Double::class -> value.toDouble() as T
	else -> throw UnsupportedOperationException("The type ${T::class.simpleName ?: T::class.toString()} is not currently supported in parameters.")
}

@PublishedApi
internal fun stringifyValue(value: Any?): String = when (value) {
	null -> value.toString()
	is String -> value
	is Boolean -> value.toString()
	is Byte -> value.toString()
	is Short -> value.toString()
	is Int -> value.toString()
	is Long -> value.toString()
	is UByte -> value.toString()
	is UShort -> value.toString()
	is UInt -> value.toString()
	is ULong -> value.toString()
	is Float -> value.toString()
	is Double -> value.toString()
	else -> throw UnsupportedOperationException("The type ${value::class.simpleName ?: value::class.toString()} is not currently supported in parameters.")
}

@PublishedApi
internal fun validateName(name: String) {
	require(name.isNotBlank()) { "The name of a parameter cannot be empty: '$name'" }
	require(name.none { it.isWhitespace() }) { "The name of a parameter cannot contain whitespace: '$name'" }
}
