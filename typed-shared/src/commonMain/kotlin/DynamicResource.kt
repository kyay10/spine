package opensavvy.spine.typed

abstract class DynamicResource(
	name: String,
	parent: Resource?,
) : Resource(name, parent)
