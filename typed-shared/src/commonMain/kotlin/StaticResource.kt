package opensavvy.spine.typed

abstract class StaticResource(
	name: String,
	parent: Resource?,
) : Resource(name, parent)
