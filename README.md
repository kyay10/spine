# Spine: Typesafe HTTP APIs with Ktor and Arrow

OpenSavvy Spine is a library to declare typesafe endpoints in code shared between your multiplatform clients and servers.

## Typed Ktor

Typed Ktor is an effort to bring type safety to multiplatform Ktor projects: by declaring endpoints and DTOs in common code, we can avoid many bugs.  

## Safe Ktor

Safe Ktor is a collection of DSLs to make error management in HTTP endpoints explicit, using Arrow.

## Spine

Spine is the combination of Typed and Safe Ktor.

## License

This project is licensed under the [Apache 2.0 license](LICENSE).

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md).
- To learn more about our coding conventions and workflow, see the [OpenSavvy Wiki](https://gitlab.com/opensavvy/wiki/-/blob/main/README.md#wiki).
- This project is based on the [OpenSavvy Playground](docs/playground/README.md), a collection of preconfigured project templates.
